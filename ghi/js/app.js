function createCard(pictureUrl, name, location_name, description, starts, ends) {
    return(`
<div class = "col" </div>
<div className="shadow p-3 mb-5 bg-body rounded"</div>

    <div className="card">
        <img src="${pictureUrl}" className="card-img-top">
        <div className="card-body">
          <h5 className="card-title">${name}</h5>
          <h7 className="card-subtitle mb-2 text-muted">${location_name}</h7>
          <p className="card-text">${description}</p>
          <div className="card-footer">${new Date(starts).toLocaleDateString()} - ${new Date(ends).toLocaleDateString()}
            </div>
        </div>
      </div>
    `);
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new error ("Bad Response");
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const pictureUrl = details.conference.location.picture_url;
                    const title = details.conference.name;
                    const location_name = details.conference.location.name;
                    const description = details.conference.description;

                    const starts = details.conference.starts;
                    const ends = details.conference.ends;

                    const html = createCard(pictureUrl,title,location_name, description,  starts, ends);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;

            }
        }
    }
} catch (e) {
        console.error(e)
    }
});
