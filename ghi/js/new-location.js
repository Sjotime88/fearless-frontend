window.addEventListener('DOMContentLoaded', async () => {



  const url = 'http://localhost:8000/api/states/';

    try {
      const response = await fetch(url);


      if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('state');
        console.log(selectTag)
        for (let state of data.states) {
          selectTag.innerHTML +=`<option value="${state.abbreviation}">${state.name}</option>`;
        };


        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
          event.preventDefault();
          const formData = new FormData(formTag);
          const json = JSON.stringify(Object.fromEntries(formData));
          const response = await fetch(url);
        });
      }
    } catch (e) {
      console.log("error")
    }
  })